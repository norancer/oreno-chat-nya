require 'chat/commands/nya'
require 'chat/handlers/base'

module Chat
  module Handlers
    class Nya < Base
      hook pattern: /(.*)/, name: 'nya', description: 'All remarks will be cat'

      def nya(message)
        Chat::Commands::Nya.new(message).call
      end
    end
  end
end
